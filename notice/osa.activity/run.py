#!/usr/bin/python3
import requests
from bs4 import BeautifulSoup

def get_page_ctx(url):
	try:
		r = requests.get(url)
		return BeautifulSoup(r.text, 'html.parser')
	except requests.exceptions.RequestException as e:
		raise SystemExit(e)

	
def get_extracurricular_activities():
	url = 'https://activity-osa.nsysu.edu.tw/p/403-1090-1137-1.php?Lang=zh-tw'
	page = get_page_ctx(url)

	pass

def get_club():
	pass

def get_finance():
	pass

def sync_2_db(a, b, c):
	pass

def alert(msg_dict):
	pass

def get_data():
	a = get_extracurricular_activities()
	b = get_club()
	c = get_finance()
	return a, b, c


if __name__ == '__main__':
	data = get_data()
	have_new, msg_dict = sync_2_db(*data)
	if have_new == True:
		alert(msg_dict)
