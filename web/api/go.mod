module main

go 1.16

require (
	github.com/badoux/checkmail v1.2.1 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/pkg/errors v0.9.1 // indirect
	go.mongodb.org/mongo-driver v1.5.0
)
