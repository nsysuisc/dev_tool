package video

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "video.html", nil)
}
