package main

import (
	"main/activity"
	shirley "main/activity/20210505"
	tim "main/activity/20210526"
	video "main/video"
	"net/http"

	"github.com/gin-gonic/gin"
)

func homeHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "home.html", nil)
}

func covid19(c *gin.Context) {
	c.HTML(http.StatusOK, "COVID19.html", nil)
}

func main() {
	server := gin.Default()
	server.LoadHTMLGlob("Template/html/*.html")
	server.Static("/assets", "./Template/assets")
	server.GET("/", homeHandler)

	server.GET("/video", video.GetHandler)
	server.GET("/COVID19", covid19)

	// Dynamic activities
	act := server.Group("activity")
	{
		act.GET("/", activity.GetHandler)
		act.GET("20210505", shirley.GetHandler)
		act.GET("20210526", tim.GetHandler)

		act.POST("20210505", shirley.FormReceiver)
		act.POST("20210526", tim.FormReceiver)
	}

	// 404 Not found
	server.NoRoute(func(c *gin.Context) {
		c.HTML(http.StatusNotFound, "404.html", nil)
	})

	// listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
	server.Run()
}
