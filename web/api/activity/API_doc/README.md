## 公開共用
固定以 GetHandler, FormReceiver 作為 GET, POST 的統一界面。

## 內部實做
### activity.go:readActivity()
以動態載入 activity.json 方式，產生活動清單表。但是這邊還有一個問題是，link 要真的寫一個 go 去接 HTTP(s)
也就是說，目前即使動態載入活動清單，實際後端還是需要重新 compile 。
#### TODO:
1. 提供一個通用版的 go，也是載入 json，以及使用 gin 的 Parameters in path 去動態生成內容。不要再重新編譯。
2. 反覆從硬碟讀檔，肯定是一件相當浪費的操作，所以需要有一個快取機制，使得優先讀取記憶體中的資料，定時更新記憶體中的內容（可能可以每天或每小時再重新讀入檔案）。


## FIXME:
1. tim.go:appendDB() 無法與 DB 連線。 500 error
```
runtime error: invalid memory address or nil pointer dereference
/usr/local/go/src/runtime/panic.go:212 (0x435a9a)
        panicmem: panic(memoryError)
/usr/local/go/src/runtime/signal_unix.go:734 (0x44e512)
        sigpanic: panicmem()
/home/scc/go/pkg/mod/go.mongodb.org/mongo-driver@v1.5.0/mongo/database.go:47 (0xb72e87)
        newDatabase: rc := client.readConcern
/home/scc/go/pkg/mod/go.mongodb.org/mongo-driver@v1.5.0/mongo/client.go:762 (0xb7c7cb)
        (*Client).Database: return newDatabase(c, name, opts...)
/media/d/git/isc/dev_tool/server/api/activity/20210526/tim.go:73 (0xb7c799)
        appendDB: coll := db.DB_Client.Database("activity").Collection("20200526")
/media/d/git/isc/dev_tool/server/api/activity/20210526/tim.go:101 (0xb7cd55)
        FormReceiver: err := appendDB(name, sid)
/home/scc/go/pkg/mod/github.com/gin-gonic/gin@v1.6.3/context.go:161 (0x8e51d0)
        (*Context).Next: c.handlers[c.index](c)
/home/scc/go/pkg/mod/github.com/gin-gonic/gin@v1.6.3/recovery.go:83 (0x8e51b7)
        RecoveryWithWriter.func1: c.Next()
/home/scc/go/pkg/mod/github.com/gin-gonic/gin@v1.6.3/context.go:161 (0x8e4253)
        (*Context).Next: c.handlers[c.index](c)
/home/scc/go/pkg/mod/github.com/gin-gonic/gin@v1.6.3/logger.go:241 (0x8e4212)
        LoggerWithConfig.func1: c.Next()
/home/scc/go/pkg/mod/github.com/gin-gonic/gin@v1.6.3/context.go:161 (0x8db38f)
        (*Context).Next: c.handlers[c.index](c)
/home/scc/go/pkg/mod/github.com/gin-gonic/gin@v1.6.3/gin.go:409 (0x8db376)
        (*Engine).handleHTTPRequest: c.Next()
/home/scc/go/pkg/mod/github.com/gin-gonic/gin@v1.6.3/gin.go:367 (0x8dae2c)
        (*Engine).ServeHTTP: engine.handleHTTPRequest(c)
/usr/local/go/src/net/http/server.go:2887 (0x6ef2e2)
        serverHandler.ServeHTTP: handler.ServeHTTP(rw, req)
/usr/local/go/src/net/http/server.go:1952 (0x6ea70c)
        (*conn).serve: serverHandler{c.server}.ServeHTTP(w, w.req)
/usr/local/go/src/runtime/asm_amd64.s:1371 (0x46dda0)
        goexit: BYTE    $0x90   // NOP
```

2. HDP 尚未完成。也尚未串接
希望是去填寫 HDP 後，會回傳一 token (有有效實現)於 reg option 欄位，後端僅校驗 token 是否有效，不做登入者關聯。

3. return http.StatusBadRequest 前端沒有跳出紅色框框