package activity

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Activity struct {
	Name     string   `json:Name`
	Host     string   `json:Host`
	Time     string   `json:Time`
	Location string   `json:Location`
	Limit    string   `json:Limit`
	Other    string   `json:Other`
	Tag      []string `json:Tag`
	Link     string   `json:Link`
}

type Activities struct {
	List []Activity `json:"activities"`
}

func readActivity() Activities {
	// TODO: Add cache machinism, file I/O is too slow
	jsonFile, err := ioutil.ReadFile("activity/activity.json")
	if err != nil {
		fmt.Println(err)
	}

	var activities Activities
	json.Unmarshal(jsonFile, &activities)
	return activities
}

func GetHandler(c *gin.Context) {

	activities := readActivity()

	insert := gin.H{
		"numActivity":  len(activities.List) - 1,
		"activityList": activities.List,
	}
	c.HTML(http.StatusOK, "activity.html", insert)
}
