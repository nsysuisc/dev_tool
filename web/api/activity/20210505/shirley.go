package shirley

import (
	"context"
	"errors"
	"log"
	db "main/db/mongo"
	"main/db/reg"
	"net/http"
	"regexp"

	"github.com/badoux/checkmail"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
)

func check_format(c *gin.Context) (int, string) {
	re := regexp.MustCompile(`^[ABCDEFGIJKLMNPQSTU]0[0-9]{8}$`)

	if in, isExist := c.GetPostForm("name"); !isExist || in == "" {
		return http.StatusBadRequest, "名字不得為空"
	}

	if in, isExist := c.GetPostForm(("sid")); !isExist || !re.MatchString(in) {
		return http.StatusBadRequest, "學號不符"
	}

	if in, isExist := c.GetPostForm(("email")); !isExist {
		err := checkmail.ValidateFormat(in)
		if err != nil {
			return http.StatusBadRequest, "請輸入正確email"
		}
	}

	if _, isExist := c.GetPostForm(("hdp")); !isExist {
		return http.StatusBadRequest, "您尚未填寫實聯制"
	}

	if _, isExist := c.GetPostForm(("reg_is_not_ac")); !isExist {
		return http.StatusBadRequest, "您尚未知悉"
	}

	return http.StatusOK, ""
}

func gen_temp_arg(succ string, err string) gin.H {
	return gin.H{
		"Title": "臺灣資安女力站出來",
		"Option": []reg.Option{
			{
				Description: "登入實聯制？",
				Item:        "同意",
				BoxName:     "hdp",
				IsLink:      true,
				Link:        "/COVID19",
			},
			{
				Description: "知悉登記不等於成功錄取？",
				Item:        "同意",
				BoxName:     "reg_is_not_ac",
			},
		},
		"Success": succ,
		"Error":   err,
		"Info": []reg.Info{
			{
				InfoText: "Email",
				InfoName: "email",
			},
		},
	}
}

func GetHandler(c *gin.Context) {
	render := gen_temp_arg("", "")
	c.HTML(http.StatusOK, "reg.html", render)
}

func appendDB(name string, sid string) error {
	new_reg := reg.Register{
		Name: name,
		Sid:  sid,
	}
	// log.Println(db.MgoCli)
	act_db := db.MgoCli.Database("activity")
	coll := act_db.Collection("20200505")

	count, err := coll.CountDocuments(context.TODO(), bson.M{"sid": new_reg.Sid})
	if count == 0 {
		coll.InsertOne(context.TODO(), new_reg)
		err = nil
	} else if count >= 1 {
		err = errors.New("repeated")
	}
	return err
}

func FormReceiver(c *gin.Context) {
	httpCode, err_msg := check_format(c)
	if err_msg != "" {
		log.Print(err_msg)
	}
	succ_msg := ""

	if httpCode == 200 {
		// Append to DB
		name, _ := c.GetPostForm("name")
		sid, _ := c.GetPostForm("sid")
		err := appendDB(name, sid)

		// If append failed
		if err != nil {
			httpCode = http.StatusBadRequest
			err_msg = "錯誤：" + err.Error()
		} else {
			succ_msg = "成功登記，敬請等候公告"
		}
	}

	render := gen_temp_arg(succ_msg, err_msg)
	c.HTML(httpCode, "reg.html", render)
}
