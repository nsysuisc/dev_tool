package reg

type Register struct {
	Name       string `bson:"name" json:"name"`
	Sid        string `bson:"sid" json:"sid"`
	Department string `bson:"department" json:"department"`
	Grade      int    `bson:"grade" json:"grade"`
}

var Registers []Register

type Info struct {
	InfoText string
	InfoName string
}

type Option struct {
	Description string
	Item        string
	BoxName     string
	IsLink      bool
	Link        string
}

// Not support update interface here
// Update what? Use Sid to update name? Or use what to update what?
// A little bit strange.
