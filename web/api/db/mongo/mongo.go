package db

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var MgoCli *mongo.Client

func init() {
	var err error
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	MgoCli, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = MgoCli.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

}
