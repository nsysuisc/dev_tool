// Template table:
// --------------------------
// @Success: string, the pop up success msg
// @Error:   string, the pop up error msg
// @Title:   string, the title of this registory event
// @Info:    []Info, the extra field to get more user info (For example: Grade and Department...)
//				   , that is, user name and sid are essential in every "activities".
// @Option:  []Option, the check box which user could select something (For example: I read...)